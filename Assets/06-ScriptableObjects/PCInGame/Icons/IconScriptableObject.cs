using UnityEngine;

[CreateAssetMenu(fileName = "IconScriptableObject", menuName = "ScriptableObjects/PCInGame")]
public class IconScriptableObject : ScriptableObject
{
    public string iconName;
    public Sprite iconSprite;
    public GameObject windowToOpen;
}
