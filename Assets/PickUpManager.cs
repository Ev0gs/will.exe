using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

[RequireComponent(typeof(MouseLook))]
public class PickUpManager : MonoBehaviour
{
    [SerializeField] private float pickUpRange = 1;
    private PickUpObject previousPickUpObject;
    [SerializeField] private LayerMask pickUpLayerMask;
    //Event pick up object event with parameter state (picked up or dropped) and object
    [SerializeField] private PickUpObject equipedObject;

    // Update is called once per frame
    void Update()
    {
        PickUpObject pickUp = null;


        if (equipedObject && equipedObject.GetCurrentState == PickUpObject.PickUpState.Inspected) return;

        if (equipedObject && equipedObject.GetCurrentState == PickUpObject.PickUpState.Plugged)
        {
            equipedObject = null;
            return;
        }

        if (equipedObject && Input.GetMouseButtonDown(0))
        {
            equipedObject.Trigger(PickUpObject.PickUpState.Dropped, out pickUp);
            equipedObject = null;
        }
        else if (Input.GetMouseButtonDown(0) && previousPickUpObject && previousPickUpObject.Isinteractable)
        {
            previousPickUpObject.Trigger(PickUpObject.PickUpState.PickedUp, out pickUp);
            equipedObject = pickUp;
        }
    }

    void FixedUpdate()
    {

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, pickUpRange, pickUpLayerMask.value))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * pickUpRange, Color.yellow);
            PickUpObject pickUpObject;
            hit.collider.TryGetComponent<PickUpObject>(out pickUpObject);
            if (pickUpObject && !pickUpObject.IsEquipped)
            {
                pickUpObject.SetInteractable(true);
                previousPickUpObject = pickUpObject;
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            if (previousPickUpObject) previousPickUpObject.SetInteractable(false);
        }
    }
}
