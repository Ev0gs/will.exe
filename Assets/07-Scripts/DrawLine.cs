using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DrawLine : MonoBehaviour
{
    public LineRenderer lineRenderer = null;
    public Camera camera = null;
    private Vector3 mousePos;
    private Vector3 pos;
    private Vector3 prevPos;
    public List <Vector3> linePositions = new List <Vector3>();
    public float minimumDistance = 0.05f;
    private float distance = 0;

    private void Update()
    {
/*        if(Input.GetMouseButtonDown(0))
        {
            mousePos = Input.mousePosition;
            mousePos.z= 0.5f;
            pos = camera.ScreenToWorldPoint(mousePos);
            prevPos= pos;
            linePositions.Add(pos);
        }
        else
        {*/
            if (Input.GetMouseButton(0))
            {
                mousePos = Input.mousePosition;
                mousePos.z = 0.5f;
                pos = camera.ScreenToWorldPoint(mousePos);
            if (linePositions.Count > 0)
            {
                distance = Vector3.Distance(prevPos, pos);
                if (distance >= minimumDistance)
                {
                    Debug.Log("pos is " + pos);
                    prevPos = pos;
                    linePositions.Add(pos);
                    lineRenderer.positionCount = linePositions.Count;
                    lineRenderer.SetPositions(linePositions.ToArray());
                }
            }
            else
            {
                prevPos = pos;
                Debug.Log("pos is " + pos);
                linePositions.Add(pos);
                lineRenderer.positionCount = linePositions.Count;
                lineRenderer.SetPositions(linePositions.ToArray());
            }
        }
       // }
    }
}
