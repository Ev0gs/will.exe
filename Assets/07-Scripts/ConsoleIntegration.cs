using UnityEngine;

class ConsoleIntegration : MonoBehaviour
{
    public void ConsoleChangedVisibility(bool visible)
    {
        if (visible)
        {
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0f;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1f;
            Cursor.visible = false;
        }
    }
}
