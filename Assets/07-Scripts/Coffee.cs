using Obi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coffee : MonoBehaviour
{
    private bool opened = false;
    private bool PlayerInZone;
    public GameObject WaterPrefab;
    ObjectPooler objectPooler;
    public PickUpObject grabCoffee;
    public GameObject spawner;
    float timer = 0.0f;
    //public ObiEmitter coffee;


    private void Start()
    {
        objectPooler = ObjectPooler.Instance;
        PlayerInZone = false;
        //coffee.enabled = false;
    }

    private void Update()
    {
        if (grabCoffee.IsEquipped && Input.GetKeyDown(KeyCode.F) && PlayerInZone == true)
        {
            //coffee.enabled = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")     //if player in zone
        {
            PlayerInZone = true;
        }
    }
    private void OnTriggerExit(Collider other)     //if player exit zone

    {
        if (other.gameObject.tag == "Player")

        {
            PlayerInZone = false;
        }
    }
}
