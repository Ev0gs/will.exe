using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tap : MonoBehaviour
{
    private bool opened = false;
    //private Animator anim;
    private bool PlayerInZone;
    public GameObject WaterPrefab;
    ObjectPooler objectPooler;


    private void Start()
    {
        PlayerInZone = false;
        objectPooler = ObjectPooler.Instance;
    }

    private void Update()
    {
        if (PlayerInZone && Input.GetKeyDown(KeyCode.F)) 
        {
            //anim = gameObject.GetComponentInParent<Animator>();
    
            opened = !opened;

            //anim.SetBool("Opened", opened);
        }
        if(opened)
        {
            objectPooler.SpawnFromPool("Cube", transform.position, Quaternion.identity);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")     //if player in zone
        {
            PlayerInZone = true;
        }
    }
    private void OnTriggerExit(Collider other)     //if player exit zone

    {
        if (other.gameObject.tag == "Player")

        {
            PlayerInZone = false;
        }
    }
}
