using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirror : MonoBehaviour
{
    public GameObject playerFocus, player, intText, focusedText;
    public bool interactable, focused;
    public MouseLook camera;
    //public GameObject draw;

    private void Start()
    {
        //draw.SetActive(false);
    }
    void OnTriggerStay(Collider other)          // if player in zone add text + can interact
    {
        intText.SetActive(true);
        interactable = true;
    }

    void OnTriggerExit(Collider other)         // if player not in zone remove text + can't interact
    {
        intText.SetActive(false);
        interactable = false;
    }

    void Update()
    {
        if (interactable == true)
        {
            if (Input.GetKeyDown(KeyCode.F)) // if player press F, he will sit down
            {
                intText.SetActive(false);
                focusedText.SetActive(true);
                playerFocus.SetActive(true);
                focused = true;
                player.SetActive(false);
                interactable = false;
                camera.enabled = false;
                Cursor.lockState = CursorLockMode.Confined;
                GetComponent<DrawLine>().enabled = true;
                //draw.SetActive(true);
            }
        }
        if (focused == true)
        {
            intText.SetActive(false);
            if (Input.GetKeyDown(KeyCode.Q)) // if player press Q and is already sitting, he will stand up
            {
                playerFocus.SetActive(false);
                focusedText.SetActive(false);
                player.SetActive(true);
                focused = false;
                Cursor.visible= false;
                GetComponent<DrawLine>().enabled = false;
                //draw.SetActive(false);
            }
        }
    }
}