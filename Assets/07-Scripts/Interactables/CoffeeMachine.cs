using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeMachine : MonoBehaviour
{
    public PickUpObject GrabCoffee;
    public PlugObject plug;
    private bool empty, PlayerInZone;
    float timer = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        GrabCoffee.enabled = false;
        empty = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (plug.plugged && Input.GetKeyDown(KeyCode.F) && empty == true)
        {
            empty = false;
            gameObject.GetComponent<AudioSource>().Play();
        }
        if (empty == false)
        {
            timer += Time.deltaTime;
            int seconds = (int)timer % 60;
            if (seconds > 5)
            {
                GrabCoffee.enabled = true;
            }
        }
    }
}
