using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PC : MonoBehaviour
{
    public GameObject player, intText, PcScreen;
    public bool interactable, focused;
    public MouseLook camera;
    public GameObject pc;
    public PlugObject plug;
    public PlayerMovement movement;

    void OnTriggerStay(Collider other)          // if player in zone add text + can interact
    {
        intText.SetActive(true);
        interactable = true;
    }

    void OnTriggerExit(Collider other)         // if player not in zone remove text + can't interact
    {
        intText.SetActive(false);
        interactable = false;
    }

    void Update()
    {
        if (plug.plugged == true)
        {
            pc.SetActive(true);
        }

        if (interactable == true && plug.plugged == true)
        {
            if (Input.GetKeyDown(KeyCode.F)) // if player press F, he will sit down
            {
                intText.SetActive(false);
                focused = true;
                interactable = false;
                camera.enabled = false;
                movement.enabled= false;
                Cursor.lockState = CursorLockMode.Confined;
                PcScreen.SetActive(true);
            }
        }
        if (focused == true)
        {
            intText.SetActive(false);
            if (Input.GetKeyDown(KeyCode.Q)) // if player press Q and is already sitting, he will stand up
            {
                focused = false;
                camera.enabled = true;
                movement.enabled= true;
                PcScreen.SetActive(false);
                pc.SetActive(true);
                Cursor.lockState = CursorLockMode.Locked;

            }
        }
    }
}
