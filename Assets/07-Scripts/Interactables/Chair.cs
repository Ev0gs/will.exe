using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chair : MonoBehaviour

{
    public GameObject playerStanding, playerSitting, intText, standText;
    public bool interactable, sitting;

    void OnTriggerStay(Collider other)          // if player in zone add text + can interact
    {
        intText.SetActive(true);
        interactable = true;
    }

    void OnTriggerExit(Collider other)         // if player not in zone remove text + can't interact
    {
        intText.SetActive(false);
        interactable = false;
    }

    void Update()
    {
        if (interactable == true)
        {
            if (Input.GetKeyDown(KeyCode.F)) // if player press F, he will sit down
            {
                intText.SetActive(false);
                standText.SetActive(true);
                playerSitting.SetActive(true);
                sitting = true;
                playerStanding.SetActive(false);
                interactable = false;
            }
        }
        if (sitting == true)
        {
            intText.SetActive(false);
            if (Input.GetKeyDown(KeyCode.Q)) // if player press Q and is already sitting, he will stand up
            {
                playerSitting.SetActive(false);
                standText.SetActive(false);
                playerStanding.SetActive(true);
                sitting = false;
            }
        }
    }
}