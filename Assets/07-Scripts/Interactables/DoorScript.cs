using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    private bool opened = false;
    private Animator anim;
    private bool PlayerInZone;

    private void Start()
    {
        PlayerInZone = false;
    }

    private void Update()
    {
        if (PlayerInZone && Input.GetKeyDown(KeyCode.F)) //if in zone and press F key
        {
            //This line will get the Animator from  Parent of the door that was hit by the raycast.
            anim = gameObject.GetComponentInParent<Animator>();

            //This will set the bool the opposite of what it is.
            opened = !opened;

            //This line will set the bool true so it will play the animation.
            anim.SetBool("Opened", opened);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")     //if player in zone
        {
            PlayerInZone = true;
        }
    }
    private void OnTriggerExit(Collider other)     //if player exit zone

    {
        if (other.gameObject.tag == "Player")

        {
            PlayerInZone = false;
        }
    }
}