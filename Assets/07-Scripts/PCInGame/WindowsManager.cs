using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowsManager : MonoBehaviour
{
    [HideInInspector] public List<GameObject> Windows = new List<GameObject>();

    public GameObject OpenWindowIconPrefab;
    public GameObject OpenWindowsSlot;
    
    private static WindowsManager instance = null;
    public static WindowsManager Instance => instance;
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void AddWindowToList(GameObject window, Sprite iconSprite)
    {
        Windows.Add(window);
        GameObject go = Instantiate(OpenWindowIconPrefab, OpenWindowsSlot.transform);
        go.transform.GetChild(0).GetComponent<Image>().sprite = iconSprite;
        //go.GetComponent<Image>().sprite = iconSprite;
    }

    public void RemoveWindowFromList(GameObject window)
    {
        Windows.Remove(window);
    }
}
