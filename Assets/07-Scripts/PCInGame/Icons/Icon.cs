using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class Icon : MonoBehaviour, IPointerClickHandler
{
    public IconScriptableObject iconScriptableObject;
    public TMP_Text iconName;

    [SerializeField] private Image image;

    [Header("DoubleClick")]
    float clicked = 0;
    float clicktime = 0;
    float clickdelay = 0.8f;
    
    private void Awake()
    {
        image.sprite = iconScriptableObject.iconSprite;
        iconName.text = iconScriptableObject.iconName;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        clicked++;
        if (clicked == 1) clicktime = Time.time;
        if (clicked > 1 && Time.time - clicktime < clickdelay)
        {
            clicked = 0;
            clicktime = 0;
            GameObject go = Instantiate(iconScriptableObject.windowToOpen, transform.parent.transform.parent.transform.parent);
            WindowsManager.Instance.AddWindowToList(go, iconScriptableObject.iconSprite);
            try
            {
                go.GetComponent<NotePad>().Init(iconName.text);
            }
            catch
            {
                
            }
        }
        else if (clicked > 2 || Time.time - clicktime > 1) clicked = 0;
    }
}
