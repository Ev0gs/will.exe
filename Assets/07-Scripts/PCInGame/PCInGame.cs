using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PCInGame : MonoBehaviour
{
    [Header("LockScreen")]
    [SerializeField] private GameObject lockScreen;

    [Header("Connection PC")]
    [SerializeField] private TMP_InputField mdpInputField;
    [SerializeField] private GameObject lockedScreen;
    [SerializeField] private GameObject desktopScreen;
    [SerializeField] private GameObject errorMessage;
    public string mdpWill = default;

    [Header("Fading")]
    [SerializeField] private GameObject blackScreen;


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            lockScreen.GetComponent<Animator>().SetTrigger("UnlockScreen");
        }
    }

    /// <summary>
    /// Log the player to the PC when connect btn clicked
    /// </summary>
    public void Login()
    {
        if (mdpInputField.text == mdpWill)
        {
            desktopScreen.SetActive(true);
            lockedScreen.SetActive(false);
            blackScreen.SetActive(true);
            blackScreen.GetComponent<Animator>().SetTrigger("FadeOut");
        }
        else
        {
            errorMessage.SetActive(true);
        }
    }
}