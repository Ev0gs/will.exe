using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class IRLDateTime : MonoBehaviour
{
    public TMP_Text Date;
    public TMP_Text Time;
    public bool isDateDigital;
    private string previousDate = string.Empty;
    private string previousTime = string.Empty;

    enum Months { January = 1, February, March, April, May, June, July, August, September, October, November, December }

    // Update is called once per frame
    void Update()
    {
        System.DateTime dateTime = System.DateTime.Now;
        string date = string.Format("{0:00}/{1:00}/{2:0000}", dateTime.Month, dateTime.Day, dateTime.Year);
        string time = string.Format("{0:00}:{1:00}", dateTime.Hour, dateTime.Minute);
        if(string.Equals(date, previousDate))
        {
            return;
        }
        else
        {
            if (!isDateDigital)
            {
                string day = string.Format("{0:00}", dateTime.Day);
                Months month = (Months)dateTime.Month;
                date = $"{dateTime.DayOfWeek}, {month} {dateTime.Day}";
            }
            Date.text = date;
            previousDate = date;
        }
        if(string.Equals(time, previousTime))
        {
            return;
        }
        else
        {
            Time.text = time;
            previousTime = time;
        }
    }
}
