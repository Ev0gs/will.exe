using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DraggableItem : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public Canvas ScreenCanvas = default;
    
    private Image image;
    private RectTransform rectTransform;
    private bool canDrag;

    [SerializeField] private CanvasGroup canvasGroup = default;
    [SerializeField] private GameObject dragBase = default;

    [HideInInspector] public Transform parentAfterDrag;

    private void Awake()
    {
        image = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left && eventData.hovered.Find((go) => go.gameObject.name == dragBase.name) != null)
        {
            canvasGroup.blocksRaycasts = false;
            parentAfterDrag = transform.parent;
            transform.SetParent(transform.root);
            transform.SetAsLastSibling();
            image.raycastTarget = false;
            canDrag = true;
        }
        else
            canDrag = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left && canDrag)
        {
            if(ScreenCanvas != null)
                rectTransform.anchoredPosition += eventData.delta / ScreenCanvas.scaleFactor;
            else
                rectTransform.anchoredPosition += eventData.delta;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left && canDrag)
        {
            transform.SetParent(parentAfterDrag);
            image.raycastTarget = true;
            canvasGroup.blocksRaycasts = true;
        }
    }
}