using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;


public class NotePad : MonoBehaviour
{
    [Header("---Icon Creation---")]
    [SerializeField] private IconScriptableObject _noteIconScriptableObject;
    [SerializeField] private GameObject _iconPrefab;
    private Transform _iconSlot;

    [Header("---UI---")]
    [SerializeField] private TMP_Text _fileNameUI;
    [SerializeField] private TMP_InputField _fileContentUI;
    [SerializeField] private GameObject _saveWindow;
    [SerializeField] private TMP_InputField _saveWindowInputField;
    [SerializeField] private GameObject _saveWindowErrorMessage;

    private string _fileName, _fileContent;
    private bool _isNew;
    private bool _isSaved = true;

    private NotePad_fileModel _fileModel = new NotePad_fileModel();

    public void Init(string noteName)
    {
        _fileName = noteName;
    }

    private void Awake()
    {
        // Get empty icon slot
        Transform t = UIManager.Instance.DesktopIconsGrid.transform;
        for (int i = 0; i < t.childCount; i++)
        {
            //Debug.Log(UIManager.Instance.DesktopIconsGrid.transform.GetChild(i).name);
            if (t.GetChild(i).childCount == 0)
            {
                _iconSlot = t.GetChild(i);
                break;
            }
        }

        // Get PC Screen canvas
        _iconPrefab.GetComponent<DraggableItem>().ScreenCanvas = UIManager.Instance.ScreenCanvas;
    }

    private void Start()
    {
        _fileContentUI.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        
        // New note
        if (GetFileContent(_fileName) == null){
            _fileNameUI.text = "Untitled - Notepad";
            _isNew = true;
        }
        // Existing note
        else
        {
            _fileNameUI.text = _fileName;
            _isNew = false;
            // Load note content
            _fileContentUI.text = GetFileContent(_fileName);
        }

        // Set local variables to separate UI from script
        _fileName = _fileNameUI.text;
        _fileContent = _fileContentUI.text;
    }

    #region Save
    /// <summary>
    /// Check if need to open saveWindow for new note
    /// </summary>
    public void Save()
    {
        if (_isNew)
        {
            _saveWindow.SetActive(true);
        }
        else
        {
            SaveFileContent();
        }
    }

    /// <summary>
    /// Save note file name and content in PlayerPrefs
    /// </summary>
    public void SaveFileContent()
    {
        NotePad_fileModelList noteFileList = GetNotepadFilesToList();
        if (_isNew)
        {
            // FileNameAlreadyExist Verification (error message)
            if (FileNameExist(noteFileList, _saveWindowInputField.text))
            {
                _saveWindowErrorMessage.SetActive(true);
                return;
            }
            else
                _saveWindowErrorMessage.SetActive(false);
            
            _fileName = _saveWindowInputField.text;
            // Set _fileModel object properties and add to list
            _fileModel.Name = _saveWindowInputField.text;
            _fileModel.Content = _fileContent;
            noteFileList.list.Add(_fileModel);
            NotePad_fileModelList n = new NotePad_fileModelList();
            n.list = noteFileList.list;

            // Convert to json string and save
            string jsonData = JsonUtility.ToJson(n);
            PlayerPrefs.SetString("Notepadfiles", jsonData);
            
            // Clear
            _isNew = false;
            Close_saveWindow();

            // Create note icon
            Icon instance = _iconPrefab.GetComponent<Icon>();
            instance.iconScriptableObject = _noteIconScriptableObject;
            GameObject go = Instantiate(_iconPrefab, _iconSlot);
            go.GetComponent<Icon>().iconName.text = _fileName;
            
        }
        else
        {
            // Content is the same
            if (String.Equals(_fileContent, GetFileContent(_fileName)))
                return;

            // Content is different
            _fileModel.Name = _fileName;
            _fileModel.Content = _fileContent;
            noteFileList.list.Where(item => item.Name == _fileModel.Name).First().Content = _fileContent;
            NotePad_fileModelList n = new NotePad_fileModelList();
            n.list = noteFileList.list;
            string jsonData = JsonUtility.ToJson(n);
            PlayerPrefs.SetString("Notepadfiles", jsonData);
        }
        _fileNameUI.text = _fileName;
        _isSaved = true;
    }

    /// <summary>
    /// Close the saveWindow for new note files
    /// </summary>
    public void Close_saveWindow()
    {
        _saveWindowInputField.text = string.Empty;
        _saveWindow.SetActive(false);
    }
    #endregion

    #region FileContentModified
    /// <summary>
    /// Check by event if note content is different
    /// </summary>
    public void ValueChangeCheck()
    {
        _fileContent = _fileContentUI.text;
        if (!_isNew && String.Equals(_fileContent, GetFileContent(_fileName)))
        {
            //Debug.Log("Same as before !");
            _isSaved = true;
            _fileNameUI.text = _fileName;
        }
        else if (_isSaved)
        {
            _isSaved = false;
            _fileNameUI.text = _fileName + "*";
        }
    }
    #endregion

    #region Utils

    /// <summary>
    /// Check if fileName is already taken
    /// </summary>
    /// <param name="filesList"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private bool FileNameExist(NotePad_fileModelList filesList, string fileName)
    {
        List<NotePad_fileModel> newList = filesList.list.Where(item => item.Name == fileName).ToList();
        if (newList.Count > 0)
            return true;
        return false;
    }

    #region NoteFilesGetters
    /// <summary>
    /// Get all note files from PlayePrefs as a list
    /// </summary>
    /// <returns></returns>
    private static NotePad_fileModelList GetNotepadFilesToList()
    {
        String jsonData = PlayerPrefs.GetString("Notepadfiles");
        NotePad_fileModelList list = JsonUtility.FromJson<NotePad_fileModelList>(jsonData);
        if (list == null)
        {
            list = new NotePad_fileModelList();
            list.list = new List<NotePad_fileModel>();
        }
        return list;
    }

    /// <summary>
    /// Get a specific note file content as string
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static string GetFileContent(string fileName)
    {
        NotePad_fileModelList noteFileList = GetNotepadFilesToList();
        try
        {
            return noteFileList.list.Where(item => item.Name == fileName).First().Content;
        }
        catch (Exception ex)
        {
            Debug.Log(ex);
            return null;
        }
    }
    #endregion

    #endregion
}


#region PlayerPrefs Data
[Serializable]
public class NotePad_fileModel
{
    public string Name;
    public string Content;
}

[Serializable]
public class NotePad_fileModelList
{
    public List<NotePad_fileModel> list;
}
#endregion
