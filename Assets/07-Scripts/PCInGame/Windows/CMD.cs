using Mono.Cecil;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CMD : MonoBehaviour
{
    /// <summary>
    /// CMD History InputField UI
    /// </summary>
    [SerializeField] private TMP_InputField _historyUI;
    /// <summary>
    /// CMD CLI InputField UI
    /// </summary>
    [SerializeField] private TMP_InputField _cliUI;
    /// <summary>
    /// History content scrollbar
    /// </summary>
    [SerializeField] private Scrollbar _scrollBar;
    /// <summary>
    /// Path of actual CMD (can be modified while moving to other folders on the PC)
    /// </summary>
    [HideInInspector] public string _sourcePrefix = "C:\\Users\\Will>";

    private void Awake()
    {
        Debug.Log(_sourcePrefix);
        // Setup History InputField to show player actual source
        _historyUI.text = $"{_sourcePrefix}\n";

        // Set scrollbar to ts max value
        SetScrollbarToEnd(_scrollBar);

        // Set listener on CLI Input Field submit
        _cliUI.onSubmit.AddListener(delegate { OnSubmit(_cliUI.text); });
    }

    private void Start()
    {
        // Setup CLI InputField for player use
        SelectInputField(_cliUI);
    }

    private void OnSubmit(string content)
    {
        // Setup CLI InputField for next command
        _cliUI.text = string.Empty;
        SelectInputField(_cliUI);

        // Add new command to History InputField
        _historyUI.text = _historyUI.text + $"{_sourcePrefix} {content}";

        // Deal with the command / Show result in History InputField
        string[] cli = content.Split(' ');
        if (cli[0] == "echo")
        {
            if(cli.Length > 1) 
            {
                string msg=string.Empty;
                for(int i=1; i<cli.Length; i++)
                {
                    msg += $"{cli[i]} ";
                }
                Echo($"\n{msg}\n\n");
            }
            else
                Echo("\n\n");
        }
        else if (content == string.Empty)
        {
            Echo("\n");
        }
        else
        {
            Error($"\n'{content}' n'est pas reconnu en tant que commande interne ou externe, un programme exécutable ou un fichier de commandes.\n\n");
        }

        // Set History InputField scrollbar position to the end
        SetScrollbarToEnd(_scrollBar);
    }

    private void Echo(string msg)
    {
        _historyUI.text = _historyUI.text + $"{msg}";
    }
    private void Error(string msg)
    {
        Echo(msg);
    }
    public static void SelectInputField(TMP_InputField inputField)
    {
        inputField.ActivateInputField();
        inputField.Select();
    }
    public static void SetScrollbarToEnd(Scrollbar scrollbar)
    {
        scrollbar.value = 1;
    }
}
