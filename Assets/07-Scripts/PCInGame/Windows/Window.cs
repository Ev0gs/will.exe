using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Window : MonoBehaviour
{
    private bool _isMaximized;
    private Vector2 _previousPosition, _nativeSize;
    private RectTransform _desktopRT, _windowRT;

    private void Start()
    {
        try
        {
            _desktopRT = UIManager.Instance.DesktopContent.GetComponent<RectTransform>();
            _windowRT = this.gameObject.transform.parent.GetComponent<RectTransform>();
            _nativeSize = _windowRT.sizeDelta;
        }
        catch(Exception ex)
        {
            Debug.LogException(ex);
        } 
    }

    public void CloseWindowBtnClick()
    {
        Destroy(this.gameObject);
        WindowsManager.Instance.RemoveWindowFromList(this.transform.parent.gameObject);
    }

    public void ResizeWindowBtnClick()
    {
        if (_isMaximized)
        {
            MinimizeWindow();
        }
        else
        {
            MaximizeWindow();
        }
        _isMaximized = !_isMaximized;
    }

    private void MaximizeWindow()
    {
        _previousPosition = _windowRT.anchoredPosition;
        _windowRT.anchoredPosition = new Vector2(0, 0);
        float width = _desktopRT.rect.width;
        float height = _desktopRT.rect.height;
        _windowRT.sizeDelta = new Vector2(width, height);
    }

    private void MinimizeWindow()
    {
        _windowRT.anchoredPosition = _previousPosition;
        _windowRT.sizeDelta = _nativeSize;
    }
}
