using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DesktopRightClickTools : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]
    private GameObject toolsPanel;
    [Header("2D Offset")]
    public float X;
    public float Y;

    public void OnPointerDown(PointerEventData eventData)
    {
        if(eventData.button == PointerEventData.InputButton.Right)
        {
            toolsPanel.transform.position = new Vector3(Input.mousePosition.x + X, Input.mousePosition.y + Y, Input.mousePosition.z);
            toolsPanel.SetActive(true);
        }
        else
        {
            toolsPanel.SetActive(false);
        }
    }
}
