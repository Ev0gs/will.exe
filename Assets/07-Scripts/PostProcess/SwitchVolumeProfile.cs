using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class SwitchVolumeProfile : MonoBehaviour
{
    [SerializeField]
    private List<VolumeProfile> volumes;

    private Volume v;
    private int index;

    private void Awake()
    {
        v = GetComponent<Volume>();

        // If Volume -> Volume profile not null
        if(v.profile.name.Length != 0)
        {
            List<VolumeProfile> newList = new List<VolumeProfile>(); 
            foreach(VolumeProfile e in volumes)
            {
                if (e.name != v.profile.name)
                {
                    newList.Add(e);
                }
            }
            newList.Insert(0, v.profile);
            volumes = newList;
        }
        // If Volume -> Volume profile null
        else
        {
            v.profile = volumes[0];
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        Debug.Log("Current Volume Profile is : " + volumes[index]);
    }

    // Update is called once per frame
    void Update()
    {
        // EXAMPLE //
        // Switch Volume Profile by ascendant order
        /*if (Input.GetKeyDown(KeyCode.Space))
        {
            index++;
            if(index >= volumes.Count)
            {
                index = 0;
            }
            v.profile = volumes[index];
        }*/
    }
}
