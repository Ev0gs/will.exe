using Obi;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.UI;

public class PlugObject : MonoBehaviour
{
    public PickUpObject grabObject;
    public bool interactable;
    public bool plugged;
    public GameObject plugText;
    public Transform plugZone;
    public MoveToTarget moveToTarget;
    Vector3 Angle;

    void Start()
    {
      interactable= false;
      plugText.SetActive(false);
      Angle = new Vector3(0, 0, 0);
    }
    void OnTriggerEnter(Collider other)
    {
        interactable = true;
        if (interactable == true && grabObject.IsEquipped)
        {
            plugText.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        interactable = false;
        plugText.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        if ( grabObject.IsEquipped && interactable == true && Input.GetKeyDown(KeyCode.F))
        {
            grabObject.Trigger(PickUpObject.PickUpState.Plugged, out PickUpObject pickUp);
            grabObject.enabled= false;
            grabObject.rb.useGravity = false;
            grabObject.transform.SetParent(plugZone.transform);
            grabObject.rb.detectCollisions = false;
            StartCoroutine(moveToTarget.Move(grabObject.gameObject, plugZone.position, 0.8f));
            grabObject.rb.constraints = RigidbodyConstraints.FreezeAll;
            grabObject.transform.rotation = Quaternion.identity;
            plugged = true;
        }
    }
}
