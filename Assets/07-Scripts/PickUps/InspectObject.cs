using Obi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI.Table;
using UnityEngine.UIElements;

[RequireComponent(typeof(PickUpObject))]
public class InspectObject : MonoBehaviour
{
    public float Speed = 100f;
    public GameObject currentObj, inspectText, backText;
    public Rigidbody objRigidbody;
    public PlayerMovement move;
    public MouseLook rotate;
    public Transform inspectZone;
    public MoveToTarget moveToTarget;
    private PickUpObject pickUpObject;
    [SerializeField] private Vector3 inspectRotation = default;

    private void Awake()
    {
        pickUpObject = GetComponent<PickUpObject>();
        pickUpObject.OnPickStateChanged += OnPickStateChanged;
    }

    private void OnPickStateChanged(PickUpObject.PickUpState previousState,PickUpObject.PickUpState newState)
    {
        if(newState == PickUpObject.PickUpState.PickedUp && ( previousState == PickUpObject.PickUpState.Dropped || previousState == PickUpObject.PickUpState.Plugged))
        {
            inspectText.SetActive(true);
        }
        else if(newState == PickUpObject.PickUpState.PickedUp && previousState == PickUpObject.PickUpState.Inspected)
        {
            StopAllCoroutines();
            currentObj.transform.rotation = Quaternion.Euler(inspectRotation);
            move.ActivateMovement(true);
            rotate.mouseSensitivity = 200;
            if (objRigidbody != null)
            {
                StartCoroutine(TogglePhysics(objRigidbody, true, 5f));
            }
            backText.SetActive(false);
            inspectText.SetActive(true);
        }
        else if (newState == PickUpObject.PickUpState.Inspected)
        {
            inspectText.SetActive(false);
            backText.SetActive(true);
            StopAllCoroutines();
            if (objRigidbody != null)
            {
                objRigidbody.isKinematic = true;
            }
            move.ActivateMovement(false);
            rotate.mouseSensitivity = 0;
            StartCoroutine(moveToTarget.Move(currentObj, inspectZone.position, 0.8f));
        }else if (newState == PickUpObject.PickUpState.Plugged || newState == PickUpObject.PickUpState.Dropped)
        {
            backText.SetActive(false);
            inspectText.SetActive(false);
        }
    }

    void Update()
    {
        if (pickUpObject.GetCurrentState == PickUpObject.PickUpState.Inspected)
        {
            float x = Input.GetAxis("Mouse X") * Speed / 4;
            float y = Input.GetAxis("Mouse Y") * Speed / 4;
            currentObj.transform.Rotate(-Vector3.up * x, Space.World);
            currentObj.transform.Rotate(Vector3.forward * y, Space.World);
        }else if (Input.GetKeyDown(KeyCode.I) && pickUpObject.GetCurrentState == PickUpObject.PickUpState.PickedUp)
        {
            pickUpObject.Trigger(PickUpObject.PickUpState.Inspected, out PickUpObject pickUp);
        }

        if (Input.GetKeyDown(KeyCode.Q) && pickUpObject.GetCurrentState == PickUpObject.PickUpState.Inspected)
        {
            pickUpObject.Trigger(PickUpObject.PickUpState.PickedUp, out PickUpObject pickUp);
        }
    }
    IEnumerator TogglePhysics(Rigidbody rb, bool value, float TimeWait)
    {
        yield return new WaitForSeconds(TimeWait);
        rb.isKinematic = !value;
    }
}
