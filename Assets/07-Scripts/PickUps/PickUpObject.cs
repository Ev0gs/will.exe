using EPOOutline;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.Events;

public class PickUpObject : MonoBehaviour
{
    public Rigidbody rb;
    public BoxCollider coll;
    public Transform player, ObjContainer, Cam;
    public float dropForwardForce, dropUpwardForce;
    private bool equipped = false;
    public bool IsEquipped { get { return equipped; } }

    public static bool slotFull;
    public Outlinable outline;
    private bool interactable;
    public bool Isinteractable { get { return interactable; } }
    private PickUpState currentState = PickUpState.Dropped;
    public PickUpState GetCurrentState { get { return currentState; } }

    public enum PickUpState
    {
        PickedUp,
        Dropped,
        Inspected,
        Plugged
    }
    public event Action<PickUpState, PickUpState> OnPickStateChanged;

    private void Awake()
    {
        rb.isKinematic = false;
        coll.isTrigger = false;
        OnPickStateChanged += OnPickStateChangedDebug;
    }
    

    private void OnPickStateChangedDebug(PickUpState peviousState, PickUpState state)
    {
        if (state == PickUpState.Inspected && !TryGetComponent<InspectObject>(out InspectObject inspectObject)) Debug.LogError($"No Inspect Object attached to this gameobject : {gameObject.name}");
    }

    public void SetInteractable(bool interactable)
    {
        this.interactable = interactable;
        outline.enabled = interactable;
    }


    public void Trigger(PickUpState targetState, out PickUpObject equipedObject)
    {
        switch (targetState)
        {
            case PickUpState.PickedUp:
                if(currentState == PickUpState.Dropped || currentState == PickUpState.Inspected || currentState == PickUpState.Plugged) PickUp();
                break;
            case PickUpState.Dropped:
                if(currentState == PickUpState.PickedUp) Drop();
                break;
            case PickUpState.Inspected:
                if (currentState == PickUpState.PickedUp) Inspect();
                break;
            case PickUpState.Plugged:
                if (currentState == PickUpState.PickedUp) Plug();
                break;
        }
        equipedObject = this;
    }

    private void Plug()
    {
        OnPickStateChanged?.Invoke(currentState,PickUpState.Plugged);
        currentState = PickUpState.Plugged;
    }

    private void PickUp()
    {
        equipped = true;
        slotFull = true;

        transform.SetParent(ObjContainer);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(Vector3.zero);

        rb.isKinematic = true;
        coll.isTrigger = true;
        OnPickStateChanged?.Invoke(currentState,PickUpState.PickedUp);
        currentState = PickUpState.PickedUp;
    }

    private void Drop()
    {
        equipped = false;
        slotFull = false;
        transform.SetParent(null);
        rb.AddForce(Cam.forward * dropForwardForce, ForceMode.Impulse);
        rb.AddForce(Cam.up * dropUpwardForce, ForceMode.Impulse);
        rb.isKinematic = false;
        coll.isTrigger = false;
        OnPickStateChanged?.Invoke(currentState,PickUpState.Dropped);
        currentState = PickUpState.Dropped;
    }

    private void Inspect()
    {
        OnPickStateChanged?.Invoke(currentState, PickUpState.Inspected);
        currentState = PickUpState.Inspected;
    }
}
