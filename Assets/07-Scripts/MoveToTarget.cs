using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToTarget : MonoBehaviour
{
    public IEnumerator Move(GameObject MovedObj, Vector3 TargetPos, float Speed = 5)
    {
        while (MovedObj.transform.position != TargetPos)
        {
            MovedObj.transform.position = Vector3.MoveTowards(MovedObj.transform.position, TargetPos, Time.deltaTime * Speed);
            yield return null;
        }
    }
}
