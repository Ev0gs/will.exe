﻿using System;

namespace SkyConsole.Utils.TypeParsers
{
    /// <summary>
    /// Parses string parameters
    /// </summary>
    [ConsoleTypeParser]
    class StringToStringParser : ITypeParser
    {
        private Type[] _types = new[] {typeof(string)};
        public Type[] SupportedTypes
        {
            get { return _types; }
        }
        public object Parse(string str, Type type, string name)
        {
            //remove quotes
            return str.Trim('"');
        }
    }
}
