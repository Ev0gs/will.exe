﻿using System;

namespace SkyConsole.Utils.TypeParsers
{
    /// <summary>
    /// Parses numbers
    /// </summary>
    [ConsoleTypeParser]
    class NumericalTypesParser : ITypeParser
    {
        private Type[] _supportedTypes = new[]
        {
            typeof(Int16),
            typeof(Int32),
            typeof(Int64),
            typeof(UInt16),
            typeof(UInt32),
            typeof(UInt64),
            typeof(Single),
            typeof(Double),
            typeof(Decimal),
            typeof(DateTime)
        };

        public Type[] SupportedTypes
        {
            get { return _supportedTypes; }
        }
        public object Parse(string str, Type type, string name)
        {
            return Convert.ChangeType(str, type);
        }
    }
}
