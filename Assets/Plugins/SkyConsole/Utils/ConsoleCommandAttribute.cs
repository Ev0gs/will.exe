﻿using System;

namespace SkyConsole.Utils
{
    /// <summary>
    /// Attribute for marking methods as command
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ConsoleCommandAttribute : Attribute
    {
        /// <summary>
        /// String name of the command, for exmaple "kill"
        /// </summary>
        public readonly string Command;
        /// <summary>
        /// Type of objects that may be handled with this command (may be null). for example "typeof(Enemy)"
        /// </summary>
        public readonly Type ObjectType;
        /// <summary>
        /// Description of the command
        /// </summary>
        public readonly string Description;

        /// <summary>
        /// Mark this method as a command handle that uses selected object
        /// </summary>
        /// <param name="command">command name</param>
        /// <param name="objectType">type of the selected object it handles (if set, function must accept it as first parameter)</param>
        /// <param name="description">description of the command</param>
        /// <example><code>[ConsoleCommandAttribute("kill", typeof(Enemy),)</code></example>
        public ConsoleCommandAttribute(string command, Type objectType=null, string description="") 
        {
            Command = command;
            ObjectType = objectType;
            Description = description;
        }
    }
}