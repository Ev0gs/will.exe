﻿using System;

namespace SkyConsole.Utils
{
    /// <summary>
    /// Marks console type parser class
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    class ConsoleTypeParserAttribute : Attribute
    {

    }
}
