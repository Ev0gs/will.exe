﻿using System;

namespace SkyConsole.Utils
{
    /// <summary>
    /// Marks console command provider class
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    class ConsoleCommandProviderAttribute : Attribute
    {
    }
}
