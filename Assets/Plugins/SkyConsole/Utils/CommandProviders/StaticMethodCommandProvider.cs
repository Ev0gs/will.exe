﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SkyConsole.Model;
using SkyConsole.Model.CommandHandlers;

namespace SkyConsole.Utils.CommandProviders
{
    /// <summary>
    /// Provides static methods marked with [ConsoleCommand]
    /// </summary>
    [ConsoleCommandProvider]
    class StaticMethodCommandProvider : ICommandProvider
    {
        /// <inheritdoc />
        public IEnumerable<IConsoleCommandHandler> GetCommands()
        {
            var staticMethods = Util.GetAssemblies()
                .SelectMany(t => t.GetTypes())
                //only static classes
                .Where(t => t.IsClass && t.IsAbstract && t.IsSealed)
                //select public static methods
                .SelectMany(t => t.GetMethods(BindingFlags.Static | BindingFlags.Public));

            foreach (var staticMethod in staticMethods)
            {
                //create command handler for each attribute (it is possible for method to have multiple attributes)
                foreach (var attr in staticMethod.GetCustomAttributes<ConsoleCommandAttribute>())
                {
                    var handler = new StaticConsoleCommandHandler(attr, staticMethod);
                    yield return handler;
                }
            }
        }
    }
}
