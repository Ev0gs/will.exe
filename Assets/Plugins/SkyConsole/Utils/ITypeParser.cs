﻿using System;

namespace SkyConsole.Utils
{
    /// <summary>
    /// Converts types from strings
    /// </summary>
    public interface ITypeParser
    {
        /// <summary>
        /// All the types parser can convert from string
        /// </summary>
        Type[] SupportedTypes { get; }

        /// <summary>
        /// Parse the object from given string to 
        /// </summary>
        /// <param name="str">string value of the parameter</param>
        /// <param name="type">target type</param>
        /// <param name="parameterName">name of the parameter</param>
        /// <returns></returns>
        object Parse(string str, Type type, string parameterName);
    }
}
