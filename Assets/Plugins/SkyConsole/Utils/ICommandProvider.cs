﻿using System.Collections.Generic;
using SkyConsole.Model;

namespace SkyConsole.Utils
{
    /// <summary>
    /// Provides commands for the console
    /// </summary>
    interface ICommandProvider
    {
        /// <summary>
        /// Get all the commands the provider has
        /// </summary>
        /// <returns>collection of command handlers</returns>
        IEnumerable<IConsoleCommandHandler> GetCommands();
    }
}
