﻿using SkyConsole.Controller;
using UnityEngine;

namespace SkyConsole.View
{
    /// <summary>
    /// Base class for views of the console
    /// </summary>
    public abstract class BaseConsoleView : MonoBehaviour
    {
        /// <summary>
        /// Controller instance to work with
        /// </summary>
        protected IConsoleController _controller;
        /// <summary>
        /// Called when console visibility changed (i.e. show/hide console)
        /// </summary>
        public abstract void OnConsoleVisibleChanged();
        /// <summary>
        /// Called when selected object changed (may be null)
        /// </summary>
        public abstract void OnSelectedObjectChanged();
        /// <summary>
        /// Called when console output has changed
        /// </summary>
        public abstract void OnLogLinesChanged();

        /// <summary>
        /// Gives controller instance to view
        /// </summary>
        /// <param name="controller"></param>
        public void BindController(IConsoleController controller)
        {
            _controller = controller;
        }
    }
}
