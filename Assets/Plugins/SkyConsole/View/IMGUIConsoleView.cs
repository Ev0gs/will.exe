﻿using System;
using SkyConsole.Controller;
using SkyConsole.Model;
using UnityEngine;
using UnityEngine.Events;

namespace SkyConsole.View
{
    /// <summary>
    /// IMGUI implementation of console view
    /// </summary>
    class IMGUIConsoleView : BaseConsoleView
    {
        /// <summary>
        /// Gui depth must be lower then any used in your game, to have overlay
        /// </summary>
        public int GuiDepth = -10;
        /// <summary>
        /// How much lines are shown
        /// </summary>
        private const int LogLinesVisible = 12;
        /// <summary>
        /// Displayed lines
        /// </summary>
        private string[] _lines = new string[0];
        /// <summary>
        /// Value of command input control
        /// </summary>
        private string _commandInputValue = "";
        /// <summary>
        /// Background texture
        /// </summary>
        private Texture2D _bgTexture;
        /// <summary>
        /// Style of output text
        /// </summary>
        private GUIStyle _logTextStyle;
        /// <summary>
        /// Style of command input control
        /// </summary>
        private GUIStyle _commandInputStyle;
        /// <summary>
        /// Style of text that describes selected object
        /// </summary>
        private GUIStyle _selectedObjectTextStyle;


        const float DesignedWidth = 960.0f;
        const float DesignedHeight = 490.0f;
        private const float DesignedFontSize = 13;
        private int _lastScreenWidth;
        private int _lastScreenHeight;
        public override void OnConsoleVisibleChanged()
        {
            //imgui does not require refresh
        }

        public override void OnSelectedObjectChanged()
        {
            //imgui does not require refresh
        }

        public override void OnLogLinesChanged()
        {
            RefreshVisibleLines();
        }

        void RefreshVisibleLines()
        {
            _lines = _controller.State.Log.GetLastLines(LogLinesVisible);
        }

        void OnGUI()
        {
            var e = Event.current;
            if (e == null)
                return;
            if (e.type == EventType.KeyDown)
            {
                HandleKeyDownEvent(e);
                if (e.character == '`' || e.character == '~')
                {
                    e.Use();
                }
            }

            if (!_controller.State.IsConsoleVisible)
                return;

            if (_lastScreenHeight != Screen.height || _lastScreenWidth != Screen.width)
            {
                _logTextStyle = _commandInputStyle = _selectedObjectTextStyle = null;
                _lastScreenWidth = Screen.width;
                _lastScreenHeight = Screen.height;
            }
            //from here, console is always visible

            var consoleRect = new Rect(0, Screen.height / 2.0f, Screen.width, Screen.height / 2.0f);
            int commandInputHeight = AdoptValueInt(30);
            const int padding = 10;

            var outputRect = new Rect(consoleRect.position + Vector2.one * padding, consoleRect.size - Vector2.one * padding * 3 + Vector2.down * commandInputHeight);
            var inputRect = new Rect(consoleRect.x + padding, outputRect.yMax + padding, outputRect.width, commandInputHeight);
            var selectedObjectRect = new Rect(Vector2.one * padding, new Vector2(outputRect.width, outputRect.y - padding*2));
            GUI.depth = GuiDepth;
            DrawBackPanel(consoleRect);
            DrawLogText(outputRect);
            DrawCommandInput(inputRect);
            DrawSelectedObjectInfo(selectedObjectRect);

            if (e.isMouse)
            {
                HandleMouseEvent(e);
            }
        }
        float GetResizeMult()
        {
            return Screen.height / DesignedHeight;
        }

        float AdoptValue(float val)
        {
            return val * GetResizeMult();
        }

        int AdoptValueInt(float val)
        {
            return Mathf.RoundToInt(AdoptValue(val));
        }
        void DrawBackPanel(Rect bgRect)
        {

            if (_bgTexture == null)
                _bgTexture = CreateTextureOfColor("#3F3F3F80");
            GUI.DrawTexture(bgRect, _bgTexture);
        }

        void DrawLogText(Rect textRect)
        {
            if (_logTextStyle == null)
            {
                _logTextStyle = new GUIStyle(GUI.skin.label);
                _logTextStyle.alignment = TextAnchor.LowerLeft;
                _logTextStyle.fontSize = AdoptValueInt(DesignedFontSize);
                //_logTextStyle.normal.background = CreateTextureOfColor("#f00");
            }
            var text = String.Join(Environment.NewLine, _lines);
            GUI.Label(textRect, text, _logTextStyle);
        }

        void DrawCommandInput(Rect inputRect)
        {
            if (_commandInputStyle == null)
            {
                _commandInputStyle = new GUIStyle(GUI.skin.textField);
                var bgTexture = CreateTextureOfColor("#2f2f2fa0");
                _commandInputStyle.active.background = bgTexture;
                _commandInputStyle.normal.background = bgTexture;
                _commandInputStyle.focused.background = bgTexture;
                _commandInputStyle.alignment = TextAnchor.MiddleLeft;
                _commandInputStyle.fontSize = AdoptValueInt(DesignedFontSize);
            }
            var name = "consoleInput";
            GUI.SetNextControlName(name);
            if (GUI.GetNameOfFocusedControl() != name)
            {
                GUI.FocusControl(name);
                var te = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
                te.OnFocus();
                te.SelectNone();
                te.MoveTextEnd();
            }

            _commandInputValue = GUI.TextField(inputRect, _commandInputValue, _commandInputStyle);

        }

        void DrawSelectedObjectInfo(Rect rect)
        {
            if (_selectedObjectTextStyle == null)
            {
                _selectedObjectTextStyle = new GUIStyle(GUI.skin.label);
                _selectedObjectTextStyle.alignment = TextAnchor.LowerLeft;
                _selectedObjectTextStyle.fontSize = AdoptValueInt(DesignedFontSize);
            }
            var o = _controller.State.SelectedObject;
            if (o == null)
            {
                return;
            }

            string str = "";

            str += "Name: " + o.name + Environment.NewLine +
                          "InstanceID: " + o.GetInstanceID();
            GUI.Label(rect, str, _selectedObjectTextStyle);

        }
        private void HandleKeyDownEvent(Event e)
        {
            if (e.keyCode == KeyCode.BackQuote)
            {
                e.Use();
                _controller.ToggleVisibility();
                return;
            }


            if (_controller.State.IsConsoleVisible)
            {

                if (e.keyCode == KeyCode.Return)
                {
                    e.Use();
                    _controller.RunCommand(_commandInputValue);
                    _commandInputValue = "";

                    return;
                }

            }
        }

        private void HandleMouseEvent(Event e)
        {
            if (e.type == EventType.MouseDown)
            {
                if (_controller.State.IsConsoleVisible)
                {
                    _controller.SelectObjectUnderClick(Input.mousePosition);
                }
            }
            e.Use();
        }

        public static Texture2D CreateTextureOfColor(string hexColor)
        {
            Color color;
            ColorUtility.TryParseHtmlString(hexColor, out color);
            return CreateTextureOfColor(color);
        }

        public static Texture2D CreateTextureOfColor(Color color)
        {
            var backgroundTex = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            backgroundTex.SetPixel(0, 0, color);
            backgroundTex.Apply();
            return backgroundTex;
        }
    }
}
