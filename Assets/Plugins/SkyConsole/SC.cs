﻿using System;
using SkyConsole.Controller;
using UnityEngine;

namespace SkyConsole
{
    public static class SC
    {
        public static IConsoleController Controller { get; set; }
        public static readonly Version Version = new Version(0,9,0);

        public static bool IsVisible
        {
            get { return Controller.State.IsConsoleVisible; }
        }

        public static bool IsHidden
        {
            get { return !IsVisible; }
        }
        public static void WriteLine(string line)
        {
            if (Controller.State.Log == null)
            {
                Debug.LogError("Cannot write to SmartConsole because Output is null");
                return;
            }
            Controller.State.Log.AddLine(line);
        }

        public static void WriteLine(object o)
        {
            if (o == null)
            {
                WriteLine("null");
                return;
            }
            WriteLine(o.ToString());
        }

        public static void WriteLine()
        {
            WriteLine("");
        }
    }
}
