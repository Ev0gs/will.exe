﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SkyConsole.Model;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SkyConsole
{
    static class Util
    {
        /// <summary>
        /// Gets all objects of given type on scene
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> GetObjectOfType<T>()
        {
            GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            return rootGameObjects.SelectMany(t => t.GetComponentsInChildren<T>());
        }
        public static string ConvertIdToString(uint id)
        {
            return Convert.ToString(id, 16).PadLeft(8, '0');
        }

        public static uint ConvertStringToId(string str)
        {
            return Convert.ToUInt32(str, 16);
        }

        public static string GetFullMethodDefinition(MethodInfo method)
        {
            if (method.DeclaringType == null)
                return method.ToString();
            string res = method.DeclaringType.Name + "." + method.Name;
            if (method.ReturnType != typeof(void))
            {
                res = method.ReturnType.Name + " " + res;
            }

            var parameters = string.Join(", ", method.GetParameters().Select(t => t.ParameterType.Name + " " + t.Name));
            res += "(" + parameters + ")";
            return res;
        }
        public static IEnumerable<Assembly> GetAssemblies()
        {
            yield return Assembly.GetExecutingAssembly();
        }
        public static IEnumerable<Type> GetAllTypesWithAttribute(Type attributeType)
        {
            return GetAssemblies().SelectMany(q=>q.GetTypes())
                .Where(t => t.GetCustomAttributes(attributeType).Any());
        }
        public static IEnumerable<GameObject> GetClickableObjectsAlongRay(Ray ray)
        {
            return GetObjectsAlongRay(ray)
                .Select(t => t.GetComponentInParent<ConsoleClickableObject>())
                .Where(t => t != null)
                .Select(t => t.gameObject)
                .Distinct();
        }
        public static IEnumerable<GameObject> GetObjectsAlongRay(Ray ray)
        {
            return HitObjects(ray).Select(t => t.transform.gameObject).Distinct();
        }
        public static IEnumerable<RaycastHit> HitObjects(Ray startRay)
        {
            var res = new List<RaycastHit>();
            var hitRay = new Ray(startRay.origin, startRay.direction);
            const int MAX_ITER = 100;
            for (int i = 0; i < MAX_ITER; i++)
            {
                RaycastHit hit;
                if (!Physics.Raycast(hitRay, out hit))
                {
                    yield break;
                }
                else
                {
                    yield return hit;
                    //move the ray
                    hitRay.origin = hit.point;
                }
            }
        }
    }
}