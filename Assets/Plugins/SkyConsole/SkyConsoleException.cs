﻿using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace SkyConsole
{
    class SkyConsoleException : Exception
    {
        public SkyConsoleException()
        {
        }

        protected SkyConsoleException([NotNull] SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public SkyConsoleException(string message) : base(message)
        {
        }

        public SkyConsoleException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
