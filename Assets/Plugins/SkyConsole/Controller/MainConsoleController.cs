﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SkyConsole.Model;
using SkyConsole.Utils;
using SkyConsole.View;
using UnityEngine;

namespace SkyConsole.Controller
{
    /// <summary>
    /// Controller implementation
    /// </summary>
    public class MainConsoleController : IConsoleController
    {
        /// <inheritdoc />
        public ConsoleState State { get; private set; }
        public BaseConsoleView View;

        public MainConsoleController(ConsoleState state, BaseConsoleView view)
        {
            State = state;
            View = view;
            InitTypeParsers();
            InitCommandContainer();

            State.Log.OnLinesChanged += View.OnLogLinesChanged;
        }
        /// <summary>
        /// Search type parses in assembly and initialize them
        /// </summary>
        private void InitTypeParsers()
        {
            var parserTypes = Util.GetAllTypesWithAttribute(typeof(ConsoleTypeParserAttribute));
            List<ITypeParser> parsers = new List<ITypeParser>();
            foreach (var parserType in parserTypes)
            {
                //check if this parser meets requirements
                var errorPrefix = "Class " + parserType.Name + " is not valid type parser : ";
                if (!parserType.GetInterfaces().Contains(typeof(ITypeParser)))
                {
                    Debug.LogError(errorPrefix + " does not implement " + nameof(ITypeParser) + " interface");
                    continue;
                }
                if (parserType.IsAbstract)
                {
                    Debug.LogError(errorPrefix + "parser cannot be static or abstract");
                    continue;
                }

                if (!parserType.GetConstructors().Any(t => t.GetParameters().Length == 0))
                {
                    Debug.LogError(errorPrefix + "does not have default constructor with no parameters");
                    continue;
                }
                try
                {
                    var parser = Activator.CreateInstance(parserType);
                    parsers.Add((ITypeParser) parser);
                }
                catch (Exception e)
                {
                    Debug.LogError("Failed to instantiate parser " + parserType.Name);
                    Debug.LogException(e);
                }
            }
            //make dictionary for better conversion performance
            var dict = new Dictionary<Type, ITypeParser>();
            foreach (var parser in parsers)
            {
                if (parser.SupportedTypes == null || parser.SupportedTypes.Length == 0)
                {
                    Debug.LogError("Parser " + parser.GetType().Name + " is invalid : SuppertedTypes is null or empty");
                    continue;
                }
                foreach (var supportedType in parser.SupportedTypes)
                {
                    dict[supportedType] = parser;
                }
            }
            State.Parsers = dict;
            //Debug.Log("Total type parsers :" + parsers.Count);
        }
        /// <summary>
        /// Initializes and populates commands container
        /// </summary>
        private void InitCommandContainer()
        {
            var container = new ConsoleCommandsContainer();
            State.Commands = container;
            //get the provides classes in assembly
            var commandProvidersTypes = Util.GetAllTypesWithAttribute(typeof(ConsoleCommandProviderAttribute));
            foreach (var commandProviderType in commandProvidersTypes)
            {
                //check the requirements
                string errorPrefix = "Class " + commandProviderType.Name + " is not valid command provider : ";
                if (!commandProviderType.GetInterfaces().Contains(typeof(ICommandProvider)))
                {
                    Debug.LogError( errorPrefix + "does not realize ICommandProvider interface");
                    continue;
                }

                if (commandProviderType.IsAbstract)
                {
                    Debug.LogError(errorPrefix + "command provider cannot be static or abstract");
                    continue;
                }

                if (!commandProviderType.GetConstructors().Any(t => t.GetParameters().Length == 0))
                {
                    Debug.LogError(errorPrefix + "does not have default constructor with no parameters");
                    continue;
                }
                //instantiate provider
                ICommandProvider provider;
                try
                {
                    provider = (ICommandProvider)Activator.CreateInstance(commandProviderType);
                }
                catch (Exception e)
                {
                    Debug.Log("Failed to handle " + commandProviderType.Name + " command provider:");
                    Debug.LogException(e);
                    continue;
                }
                //retrieve commands from provider
                IConsoleCommandHandler[] commands;
                try
                {
                    commands = provider.GetCommands().ToArray();
                    
                }
                catch (Exception e)
                {
                    Debug.Log("Failed to get commands from " + commandProviderType.Name + " command provider");
                    Debug.LogException(e);
                    continue;
                }
                //add commands to container
                container.Handlers.AddRange(commands);
            }
            Debug.Log("total commands: " + container.Handlers.Count);
        }
        /// <summary>
        /// Executes given command string
        /// </summary>
        /// <param name="commandStringInput"></param>
        public void RunCommand(string commandStringInput)
        {
            SC.WriteLine(">" + commandStringInput);
            //string sentence = "This is a sentence with multiple    spaces";
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);
            var commandString = regex.Replace(commandStringInput, " ");
            if (commandString != commandStringInput)
            {
                SC.WriteLine("Removed double spaces");
                SC.WriteLine(">" + commandString);
            }


            var arr = commandString.Split(' ');
            if (arr.Length < 1)
            {
                Debug.Log("Empty command string");
                return;
            }

            var cmdName = arr[0];
            var arguments = arr.Skip(1).ToArray();

            var cmd = State.Commands.FindHandler(cmdName, arguments.Length, GetTypesOfObject(State.SelectedObject));
            if (cmd == null)
            {
                SC.WriteLine("Command \"" + cmdName + "\" not found");
                return;
            }
            object selected = null;
            if (cmd.ObjectType != null)
                selected = State.SelectedObject.GetComponent(cmd.ObjectType);
            try
            {
                var parametersConverted = PrepareParametersForCommand(arguments, cmd);
                var res = cmd.Execute(selected, parametersConverted);
                if (res != null)
                {
                    SC.WriteLine(res);
                }
            }
            catch (SkyConsoleException e)
            {
                SC.WriteLine("Error : " + e.Message);
            }
        }
        /// <summary>
        /// Converts strings to objects, according to types in cmd
        /// </summary>
        /// <param name="values"></param>
        /// <param name="cmd"></param>
        /// <returns></returns>
        private object[] PrepareParametersForCommand(string[] values, IConsoleCommandHandler cmd)
        {
            var res = new object[values.Length];
            for (int i = 0; i < res.Length; i++)
            {
                var str = values[i];
                var type = cmd.Parameters[i];
                //TODO: may be problem that there is no parser for this type
                //or this check may be implemented at commands initialization
                ITypeParser parser;
                if (!State.Parsers.TryGetValue(type.Type, out parser))
                {
                    throw new SkyConsoleException("No parser found for type " + type.Type.Name);
                }
                //TODO: inheritance?
                try
                {
                    res[i] = parser.Parse(str, type.Type, type.Name);
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    throw new SkyConsoleException("Failed to parse " + type.Type.Name + " from \"" + str + "\" : " + e.Message, e);
                }
            }

            return res;
        }
        /// <summary>
        /// Get types of all components the gameobject has
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private Type[] GetTypesOfObject(GameObject o)
        {
            if (o == null)
            {
                return null;
            }
            return o.GetComponents<Component>().Select(t => t.GetType()).Distinct().ToArray();
        }
        /// <inheritdoc />
        public void ToggleVisibility()
        {
            State.IsConsoleVisible = !State.IsConsoleVisible;
            View.OnConsoleVisibleChanged();
            //invoke events
            if (State.IsConsoleVisible)
            {
                State.OnConsoleShown.Invoke();
            }
            else
            {
                State.OnConsoleHidden.Invoke();
            }
        }
        /// <inheritdoc />
        public void SelectObjectUnderClick(Vector3 mousePos)
        {
            if (!State.IsConsoleVisible)
            {
                throw new Exception("Cannot get object under click - console is not visible");
            }
            
            var objects = GetObjectsUnderClick(mousePos);

            var newSelectedObject = objects.FirstOrDefault();

            if (State.SelectedObject != newSelectedObject)
            {
                State.SelectedObject = newSelectedObject;
                View.OnSelectedObjectChanged();
            }
        }
        /// <summary>
        /// Gets objects under click
        /// </summary>
        /// <param name="mousePos"></param>
        /// <returns></returns>
        private IEnumerable<GameObject> GetObjectsUnderClick(Vector3 mousePos)
        {
            Ray ray = Camera.main.ScreenPointToRay(mousePos);
            return Util.GetClickableObjectsAlongRay(ray).ToArray();
        }
    }
}