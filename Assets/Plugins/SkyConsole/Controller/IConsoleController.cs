﻿using SkyConsole.Model;
using UnityEngine;

namespace SkyConsole.Controller
{
    /// <summary>
    /// Console controller interface. This methods may be called from the view
    /// </summary>
    public interface IConsoleController
    {
        /// <summary>
        /// The state of console
        /// </summary>
        ConsoleState State { get; }
        /// <summary>
        /// Called to show/hide visibility of console
        /// </summary>
        void ToggleVisibility();
        /// <summary>
        /// Execute given command string
        /// </summary>
        /// <param name="commandString">user input to the console</param>
        void RunCommand(string commandString);
        /// <summary>
        /// Selects object under mouse click
        /// </summary>
        /// <param name="mousePos">position of mouse on the screen</param>
        void SelectObjectUnderClick(Vector3 mousePos);
    }
}
