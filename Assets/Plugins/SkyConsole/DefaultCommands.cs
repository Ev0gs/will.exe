﻿using System.Collections.Generic;
using System.Linq;
using SkyConsole.Model;
using SkyConsole.Utils;
using UnityEngine;

namespace SkyConsole
{
    static class DefaultCommands
    {
        [ConsoleCommand("help", description: "Shows list of available commands and their parameters")]
        public static void Help()
        {
            SC.WriteLine("Availale commands:");
            var commands = SC.Controller.State.Commands.Handlers;
            foreach (var command in commands)
            {
                string line = command.Name;
                var parameters = GetParameters(command).ToArray();
                if (parameters.Length > 0)
                {
                    line += " " + string.Join(" ", parameters);
                }
                if (!string.IsNullOrEmpty(command.Description))
                {
                    line += " : " + command.Description;
                }
                SC.WriteLine(line);
            }
        }
        [ConsoleCommand("hello", description:"Just writes hello world")]
        public static void Hello()
        {
            SC.WriteLine("Hello world!");
        }
        [ConsoleCommand("help", description: "Shows detailed description of specific command")]
        public static void Help(string commandName)
        {
            var commands = SC.Controller.State.Commands.Handlers.Where(t => t.Name == commandName).ToArray();
            if (commands.Length == 0)
            {
                SC.WriteLine("No command found with name \"" + commandName + "\"");
                return;
            }

            if (commands.Length == 1)
            {
                DescribeCommand(commands[0]);
                return;
            }
            SC.WriteLine("Found " + commands.Length + " overloads with name \"" + commandName + "\"");
            foreach (var cmd in commands)
            {
                DescribeCommand(cmd);
                SC.WriteLine();
            }
            
        }

        private static void DescribeCommand(IConsoleCommandHandler command)
        {
            var parameterNames = GetParameters(command);
            SC.WriteLine(command.Name + " " + string.Join(" ", parameterNames));
            SC.WriteLine("\t" + command.Description);
            SC.WriteLine("\tMethod signature: " + command.FullFunctionPath);
        }

        private static IEnumerable<string> GetParameters(IConsoleCommandHandler command)
        {
            return command.Parameters.Select(t => "<" + t.Name + ">");
        }
        
    }
}
