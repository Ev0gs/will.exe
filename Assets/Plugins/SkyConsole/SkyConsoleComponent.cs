﻿using SkyConsole.Controller;
using SkyConsole.Model;
using SkyConsole.View;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace SkyConsole
{
    /// <summary>
    /// Component that makes console parts work together
    /// </summary>
    public class SkyConsoleComponent : MonoBehaviour
    {
        public MainConsoleController controller;
        public ConsoleState state;
        /// <summary>
        /// Defines the view console will use
        /// </summary>
        public BaseConsoleView view;
        /// <summary>
        /// Called when console is shown
        /// </summary>
        public UnityEvent OnConsoleShown;
        /// <summary>
        /// Called when console is hidden
        /// </summary>
        public UnityEvent OnConsoleHidden;
        void Awake()
        {
            ConsoleState state = new ConsoleState();
            state.OnConsoleShown = OnConsoleShown;
            state.OnConsoleHidden = OnConsoleHidden;
            controller = new MainConsoleController(state, view);
            view.BindController(controller);
            SC.Controller = controller;
            //hide console if it is visible
            view.OnConsoleVisibleChanged();

            var message = "SkyConsole ";
            message += "Lite ";
            message += "v" + SC.Version.ToString();
            SC.WriteLine(message);
        }
    }
}
