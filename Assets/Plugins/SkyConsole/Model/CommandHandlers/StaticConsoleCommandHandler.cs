﻿using System;
using System.Linq;
using System.Reflection;
using SkyConsole.Utils;

namespace SkyConsole.Model.CommandHandlers
{
    /// <summary>
    /// Command handler for static method commands
    /// </summary>
    public class StaticConsoleCommandHandler : IConsoleCommandHandler
    {
        /// <inheritdoc />
        public string Name { get; private set; }

        /// <inheritdoc />
        public Type ObjectType { get; private set; }

        /// <inheritdoc />
        public CommandParameter[] Parameters { get; private set; }

        /// <inheritdoc />
        public string Description { get; private set; }

        public string FullFunctionPath
        {
            get { return Util.GetFullMethodDefinition(_method); }
        }

        private readonly ConsoleCommandAttribute _attribute;
        private readonly MethodInfo _method;

        public StaticConsoleCommandHandler(string name, Type objectType,string description, ConsoleCommandAttribute attribute, MethodInfo method)
        {
            Name = name;
            ObjectType = objectType;
            Description = description;
            _attribute = attribute;
            _method = method;
            Parameters = method.GetParameters()
                .Select(t => new CommandParameter(t.ParameterType, t.Name))
                //skip first parameter, if its method uses selected object
                .Skip(objectType == null?0:1)
                .ToArray();
        }

        public StaticConsoleCommandHandler(ConsoleCommandAttribute attribute, MethodInfo methodInfo)
            :this(attribute.Command, attribute.ObjectType, attribute.Description, attribute, methodInfo) { }

        /// <inheritdoc />
        public object Execute(object selected, object[]parameters)
        {
            if (ObjectType == null)
            {
                //this command does not require selected object as a reference
                return _method.Invoke(null, parameters);

            }
            else
            {
                var args = new[] {selected}.Concat(parameters).ToArray();
                return _method.Invoke(null, args);
            }
        }

        public override string ToString()
        {
            return FullFunctionPath;
        }
    }
}
