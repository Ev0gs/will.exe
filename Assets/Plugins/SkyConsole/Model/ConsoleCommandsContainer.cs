﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SkyConsole.Model
{
    /// <summary>
    /// Stores and helps to find appropriate command
    /// </summary>
    public class ConsoleCommandsContainer
    {
        private List<IConsoleCommandHandler> _handlers = new List<IConsoleCommandHandler>();

        public List<IConsoleCommandHandler> Handlers
        {
            get { return _handlers; }
        }
        /// <summary>
        /// Find handler for given parameters
        /// </summary>
        /// <param name="command">command name</param>
        /// <param name="parametersCount">count of provided parameters</param>
        /// <param name="selectedObjectTypes">types of components of selected game object</param>
        /// <returns></returns>
        public IConsoleCommandHandler FindHandler(string command, int parametersCount, Type[] selectedObjectTypes = null)
        {
            var filtered = Handlers.Where(t => t.Name == command && t.Parameters.Length == parametersCount);
            if (selectedObjectTypes != null)
            {
                //select only commands that support given types
                filtered = filtered.Where(t => (
                    //type of the command is exact as one of given types
                    selectedObjectTypes.Contains(t.ObjectType) || 
                    //type of the command is parent of one of given types
                    selectedObjectTypes.Any(q=>q.IsSubclassOf(t.ObjectType)) || 
                    //this command does not require object selected
                    t.ObjectType == null
                    ));
            }
            else
            {
                //select only commands without selected object support
                filtered = filtered.Where(t => t.ObjectType == null);
            }

            var res = filtered.ToArray();
            if (res.Length == 0)
            {
                //not found
                return null;
            }
            if (res.Length > 1)
            {
                SC.WriteLine("Found more than one command:");
                foreach (var consoleCommandHandler in res)
                {
                    SC.WriteLine(consoleCommandHandler);
                }
                SC.WriteLine("Using first function");
            }

            return res[0];
        }
    }
}
