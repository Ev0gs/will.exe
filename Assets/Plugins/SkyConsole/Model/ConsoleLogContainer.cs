﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SkyConsole.Model
{
    /// <summary>
    /// Stores console output data
    /// </summary>
    public class ConsoleLogContainer
    {
        /// <summary>
        /// All the lines in the log (including commands themselves)
        /// </summary>
        private List<string> _lines = new List<string>();
        /// <summary>
        /// Called when lines collection is changed
        /// </summary>
        public event Action OnLinesChanged;
        /// <summary>
        /// Adds line to the log
        /// </summary>
        /// <param name="line"></param>
        public void AddLine(string line)
        {
            _lines.Add(line);
            OnLinesChanged?.Invoke();
        }

        /// <summary>
        /// Get last n lines
        /// </summary>
        /// <param name="count"></param>
        /// <param name="offset">offset from the end</param>
        /// <returns></returns>
        public string[] GetLastLines(int count, int offset = 0)
        {
            //TODO: may be not optimal
            return _lines.Skip(_lines.Count - count - offset).Take(count).ToArray();
        }
        /// <summary>
        /// Count of lines 
        /// </summary>
        /// <returns></returns>
        public int GetLinesCount()
        {
            return _lines.Count;
        }
    }
}
