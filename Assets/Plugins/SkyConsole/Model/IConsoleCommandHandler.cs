﻿using System;

namespace SkyConsole.Model
{
    /// <summary>
    /// Describes and stores one console command
    /// </summary>
    public interface IConsoleCommandHandler
    {
        /// <summary>
        /// Name of the command (that is used in console)
        /// </summary>
        string Name { get; }
        /// <summary>
        /// Type of object to be selected
        /// </summary>
        Type ObjectType { get; }
        /// <summary>
        /// Types of parameters of command (if array empty - no parameters)
        /// </summary>
        CommandParameter[] Parameters { get; }
        /// <summary>
        /// Description of the command
        /// </summary>
        string Description { get; }
        /// <summary>
        /// Gives the full path to method to which the command is bound
        /// </summary>
        string FullFunctionPath { get; }

        /// <summary>
        /// Run the command
        /// </summary>
        /// <param name="selected"></param>
        /// <param name="parameters"></param>
        /// <returns>returned value from command (may be null/void)</returns>
        object Execute(object selected, object[] parameters);
    }
}
