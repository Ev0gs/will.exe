﻿using UnityEngine;

namespace SkyConsole.Model
{
    /// <summary>
    /// Add this components to objects that can be selected with mouse from console view
    /// </summary>
    public class ConsoleClickableObject : MonoBehaviour
    {

    }
}
