﻿using System;
using System.Collections.Generic;
using SkyConsole.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace SkyConsole.Model
{
    /// <summary>
    /// Defines the state (model) of the console
    /// </summary>
    public class ConsoleState
    {
        /// <summary>
        /// Object selected with a click
        /// </summary>
        public GameObject SelectedObject { get; set; }
        /// <summary>
        /// Is console visible now or not
        /// </summary>
        public bool IsConsoleVisible { get; set; }
        /// <summary>
        /// Container with available commands
        /// </summary>
        public ConsoleCommandsContainer Commands { get; set; }
        /// <summary>
        /// Type parsers
        /// </summary>
        public Dictionary<Type, ITypeParser> Parsers { get; set; }
        /// <summary>
        /// Container with lines written in console
        /// </summary>
        public ConsoleLogContainer Log { get; set; } = new ConsoleLogContainer();

        /// <summary>
        /// Called when console is shown
        /// </summary>
        public UnityEvent OnConsoleShown;
        /// <summary>
        /// Called when console is hidden
        /// </summary>
        public UnityEvent OnConsoleHidden;


    }
}
