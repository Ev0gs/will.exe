﻿using System;

namespace SkyConsole.Model
{
    /// <summary>
    /// Describes one command parameter
    /// </summary>
    public class CommandParameter
    {
        /// <summary>
        /// Type of the parameter
        /// </summary>
        public Type Type { get; set; }
        /// <summary>
        /// Name of the parameter
        /// </summary>
        public string Name { get; set; }

        public CommandParameter()
        {
        }

        public CommandParameter(Type type, string name)
        {
            Type = type;
            Name = name;
        }
    }
}
