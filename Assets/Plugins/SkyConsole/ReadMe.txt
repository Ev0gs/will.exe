Hello!
This asset provides an in-game console for your unity project.
Read the manual.pdf file to understand how to integrate and use SkyConsole

Please, do not forget to write a review if you like it!
If you have any questions, problems or ideas, contact me:
alexsv1802@gmail.com
--------------
Best regards,
Alex Sveredyuk