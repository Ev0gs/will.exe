using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
	public AudioClip MainMusic = default;
	public AudioSource EffectsSource = default;
	public AudioSource MusicSource = default;
	public float LowPitchRange = .95f;
	public float HighPitchRange = 1.05f;
	public static SoundManager Instance = null;


	private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
	}

    private void Start()
    {
		this.PlayMusic(this.MainMusic);
	}

	public void Play(AudioClip clip)
	{
		EffectsSource.clip = clip;
		EffectsSource.Play();
	}
	public void PlayMusic(AudioClip clip)
	{
		MusicSource.clip = clip;
		MusicSource.loop = true;
		MusicSource.Play();
	}
	public void RandomSoundEffect(params AudioClip[] clips)
	{
		int randomIndex = Random.Range(0, clips.Length);
		float randomPitch = Random.Range(LowPitchRange, HighPitchRange);
		EffectsSource.pitch = randomPitch;
		EffectsSource.clip = clips[randomIndex];
		EffectsSource.Play();
	}

	public float GetSFXVolume()
    {
		return this.EffectsSource.volume;
    }

	public float GetMusicVolume()
	{
		return this.MusicSource.volume;
	}

	public void SetMusicVolume(float newVolume)
	{
		this.MusicSource.volume = newVolume;
	}
	public void SetSFXVolume(float newVolume)
	{
		this.EffectsSource.volume = newVolume;
	}
}
