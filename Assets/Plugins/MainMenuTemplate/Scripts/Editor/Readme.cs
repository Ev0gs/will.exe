using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Helpers
{
    [AddComponentMenu("Dev Tools/Readme")]
    public class Readme : MonoBehaviour
    {
        public bool isReady = true;
        public string TextTitle = "Type your title here";
        public string TextInfo = "Type your message here";

        private void Awake()
        {
            this.enabled = false; // Disable this component when game start
        }

        public void SwitchToggle()
        {
            isReady = !isReady;
        }
    }

    [CustomEditor(typeof(Readme))]
    public class InspectorReadme : Editor
    {
        private string buttonText = "Start typing";
        private GUIStyle guiStyle = new GUIStyle();
        private GUIStyle titleGuiStyle = new GUIStyle();

        public override void OnInspectorGUI()
        {
            Readme readme = (Readme)target;
            guiStyle.fontSize = 20;
            guiStyle.alignment = TextAnchor.UpperLeft;
            guiStyle.wordWrap = true;
            titleGuiStyle.fontSize = 20;
            titleGuiStyle.alignment = TextAnchor.MiddleCenter;

            if (!readme.isReady)
            {
                EditorGUILayout.LabelField(readme.TextTitle, titleGuiStyle);
                EditorGUILayout.LabelField("");
                EditorGUILayout.LabelField(readme.TextInfo, guiStyle);
                EditorGUILayout.LabelField("");
                if (GUILayout.Button(buttonText)) readme.SwitchToggle();
                {
                    buttonText = "Change";

                }
            }
            else
            {



                EditorGUILayout.LabelField("Title :", EditorStyles.boldLabel);
                readme.TextTitle = EditorGUILayout.TextArea(readme.TextTitle, EditorStyles.textArea);

                EditorGUILayout.LabelField("Text : ", EditorStyles.boldLabel);
                readme.TextInfo = EditorGUILayout.TextArea(readme.TextInfo, EditorStyles.textArea);


                buttonText = "Save";
                if (GUILayout.Button(buttonText)) readme.SwitchToggle();
            }
        }
    }

}
