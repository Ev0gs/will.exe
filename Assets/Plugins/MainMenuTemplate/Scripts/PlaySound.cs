using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    [SerializeField] private AudioClip clipToPlay = default;   
    public void PlaySFX()
    {
        Debug.Log("Play sound from : " + this.gameObject.name);
        SoundManager.Instance.Play(clipToPlay);
    }

}
