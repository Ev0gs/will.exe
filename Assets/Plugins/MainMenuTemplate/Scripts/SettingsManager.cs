using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    [SerializeField] private Slider SFXSlider = default;
    [SerializeField] private Slider MusicSlider = default;

    void Start()
    {
        SFXSlider.value = SoundManager.Instance.GetSFXVolume();
        MusicSlider.value = SoundManager.Instance.GetMusicVolume();
    }

    public void SetSFXVolume()
    {
        SoundManager.Instance.SetSFXVolume(SFXSlider.value);
    }
    public void SetMusicVolume()
    {
        SoundManager.Instance.SetMusicVolume(MusicSlider.value);
    }
}
