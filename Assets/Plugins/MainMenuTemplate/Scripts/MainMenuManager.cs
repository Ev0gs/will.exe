using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public void Play()
    {
        SceneManager.LoadScene("SC_Play");
    }

    public void LoadSettings()
    {
        SceneManager.LoadScene("SC_Settings");
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
